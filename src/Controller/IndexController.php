<?php

namespace App\Controller;


use App\Entity\User;
use App\Form\UserType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{


    /**
     * @Route("/", name="home")
     */
    public function indexAction(ApiContext $context)
    {
        return $this->render('index.html.twig', []);

    }


    /**
     * @Route("/sign_up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword()
                    ];

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);
                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("ping");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }



    /**
     * @Route("/authenticate", name="authenticate")
     * @Method({"GET","HEAD","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function authenticateAction(Request $request, UserRepository $userRepository, ApiContext $apiContext)
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();
            $username = $data['email'];
            $password = $data['password'];

            $error = '';

            try {
                if ($apiContext->clientExists($username, $username)) {
                    $authed_user = $userRepository->findOneByUsername($username);
                    $token = new UsernamePasswordToken($authed_user, null, 'main', $authed_user->getRoles());
                    $this->container->get('security.token_storage')->setToken($token);
                    $this->container->get('session')->set('_security_main', serialize($token));

                    return $this->render('start_page.html.twig', array(
                        'user' => $authed_user,
                        'error' => $error
                    ));
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage().'  |||  '.var_export($e->getResponse(),1);
            }
        }

        return $this->render('login.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/social-authenticate-back", name="app_social_authenticate_back")
     */
    public function socialAuthenticateBackAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя

        dump($user);

        return $this->render('index.html.twig', []);

    }

    /**
     * @Route("/ping", name="ping")
     *
     * @param ApiContext $context
     * @return Response
     */
    public function pingAction(ApiContext $context)
    {
        try{
            $result = $context->makePing();
            return new Response(var_export($result,1));
        } catch (ApiException $e){
            return new Response($e->getMessage());
        }
    }

    /**
     * @Route("/qwe", name="qwe")
     *
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('passport 111', 'kkk@kkk.ru');

        return new Response(var_export($result, 1));
    }
}
