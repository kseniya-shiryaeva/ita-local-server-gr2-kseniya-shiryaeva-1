<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 17.06.18
 * Time: 9:25
 */

namespace App\Model\Api;


class ApiException extends \Exception
{
    /**
     * @var mixed
     */
    private $response;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, $response = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse() {
        return $this->response;
    }
}